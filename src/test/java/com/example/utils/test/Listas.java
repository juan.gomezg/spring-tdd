package com.example.utils.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Listas {
	
	ArrayList<String> estudiantes = new ArrayList<String>();
	
	@BeforeEach
	void init() {
		estudiantes.clear();
		estudiantes.add("Daniel");
		estudiantes.add("Juan");
		estudiantes.add("Marcos");
		estudiantes.add("Natalia");
		estudiantes.add("Camila");
		estudiantes.add("Nicolas");
		
	}

	@Test
	void testAgregarEstudianteLista() {
		estudiantes.add("Antony");
		assertEquals("Antony", estudiantes.get(estudiantes.size()-1));
	}
	
	@Test
	void testBuscarUnEstudiante() {
		String realidad = estudiantes.get(5);
		assertEquals("Nicolas", realidad );
	}
	
	@Test
	void testEliminarEstudiantes() {
		estudiantes.clear();
		assertTrue(estudiantes.isEmpty());
	}
	
}
