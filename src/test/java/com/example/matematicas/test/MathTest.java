package com.example.matematicas.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import com.example.matematicas.Calculadora;


public class MathTest {
	
	@Test
	void testValidarSumaDosValores() {
		Calculadora calculadora = new Calculadora();
		int expectativa = 15;
		int realidad = calculadora.suma(5, 10);
		assertEquals(expectativa, realidad, "El metodo deberia sumar dos numeros");
	}
	
	@Test
	void testCalcularAreaCircunferencia() {
		Calculadora calculadora = new Calculadora();
		double expectativa = 314.16;
		double realidad = calculadora.areaCircunferencia(10.0);
		assertEquals(expectativa, realidad, 0.5,"El metodo deberia hallar el area de la circunferencia");
	}
	
}