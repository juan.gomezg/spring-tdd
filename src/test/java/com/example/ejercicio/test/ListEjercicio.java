package com.example.ejercicio.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import com.example.entities.Estudiante;
import com.example.utilidades.Ejercicio;

public class ListEjercicio {

	@Test
	void testOrdenarListaEstudiantes() {
		
		ArrayList<Estudiante> arrayEstudiante = new ArrayList<Estudiante>();
		
		Estudiante estudiante1 = new Estudiante("Antony","Gomez",2251175,"juan.gomezg@usantoto.edu.co");
		Estudiante estudiante2 = new Estudiante("Zamir","Gomez",2251176,"luis.gomez@usantoto.edu.co");
		Estudiante estudiante3 = new Estudiante("Oscar","Gomez",2251177,"oscar.gomez@usantoto.edu.co");
		Estudiante estudiante4 = new Estudiante("Cristhian","Camargo",2251178,"Cristhian.Camargo@usantoto.edu.co");
		Estudiante estudiante5 = new Estudiante("Jose","Garcia",2251179,"jose.garcia@usantoto.edu.co");
		
		arrayEstudiante.add(estudiante1);
		arrayEstudiante.add(estudiante2);
		arrayEstudiante.add(estudiante3);
		arrayEstudiante.add(estudiante4);
		arrayEstudiante.add(estudiante5);
		
		
		Ejercicio ejercicio = new Ejercicio();
		arrayEstudiante = ejercicio.ordenarPorNombre(arrayEstudiante);
		assertEquals("Antony", arrayEstudiante.get(0).getNombre());
		assertEquals("Zamir", arrayEstudiante.get(arrayEstudiante.size()-1).getNombre());
		
	}
	
}
