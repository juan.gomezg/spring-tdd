package com.example.matematicas;

public class Calculadora {

	public int suma(int a, int b) {
		return a+b;
	}
	
	public double areaCircunferencia(double radio) {
		return Math.PI*Math.pow(radio, 2);
	}
	
}
