package com.example.utilidades;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.example.entities.Estudiante;

public class Ejercicio {

	public ArrayList<Estudiante> ordenarPorNombre (ArrayList<Estudiante> arrayList){
		
		Collections.sort(arrayList, new Comparator<Estudiante>() {
			
			@Override
			public int compare(Estudiante o1, Estudiante o2) {
				return o1.getNombre().compareTo(o2.getNombre());
			}
		});
		
		return arrayList;
	}
	
}
